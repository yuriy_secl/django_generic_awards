# Generated by Django 3.0.8 on 2020-07-28 13:30

import django.contrib.sites.managers
from django.db import migrations
import django.db.models.manager


class Migration(migrations.Migration):

    dependencies = [
        ('django_generic_awards', '0007_imageattachment'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='award',
            managers=[
                ('award_manager', django.db.models.manager.Manager()),
                ('objects', django.contrib.sites.managers.CurrentSiteManager()),
            ],
        ),
    ]
