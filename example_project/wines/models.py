from django.db.models import BooleanField, CharField, ForeignKey, IntegerField, TextField, SET_DEFAULT
from django.utils.translation import gettext_lazy as _

from django_generic_awards.models import Model

class Wine(Model):
    """Stores a single Wine entry."""

    class Meta:
        verbose_name = _('wine')
        verbose_name_plural = _('wines')

    descriptive_title = CharField(_('Title'), max_length=255, default='', blank=True, null=False)
    description = TextField(_('Description'), null=True, blank=True)
    active = BooleanField(_('Active'), default=False)
    sort = IntegerField(_('Sort'), default=0, blank=True, null=True)

    def __str__(self):
        return self.descriptive_title
