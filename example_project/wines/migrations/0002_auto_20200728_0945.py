# Generated by Django 3.0.8 on 2020-07-28 09:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wines', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='wine',
            options={'verbose_name': 'wine', 'verbose_name_plural': 'wines'},
        ),
    ]
