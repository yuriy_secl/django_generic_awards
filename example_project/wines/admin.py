from django.contrib import admin

from tof.admin import TofAdmin

from wines.models import Wine


@admin.register(Wine)
class WineAdmin(TofAdmin):
    pass
